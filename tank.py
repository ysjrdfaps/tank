import random

import pygame
from pygame.locals import *

from bomb import Bomb
from missile import Missile


class Tank(pygame.sprite.Sprite):
    """
    坦克类，包括敌我双方坦克
        参数：
        x,y 坦克坐标
        type 坦克类型，0为敌方坦克，1为英雄1号，2为英雄2号
        tg main主类引用
    """

    def __init__(self, x, y, type, tg):
        pygame.sprite.Sprite.__init__(self)
        self.tg = tg
        self.type = type
        self.is_super = False # 无敌状态
        self.img_num = self.type if self.type > 0 else random.randrange(0,4,3)
        self.images = self.tg.images.tank[self.img_num]
        self.image = self.images.get('up')
        self.rect = self.image.get_rect()
        self.mask = pygame.mask.from_surface(self.image)
        # 坦克速度由主类通过难度和关卡计算
        self.speed = self.tg.tankspeed
        self.dirs = 'up', 'down', 'left', 'right', 'stop'
        # 坦克方向
        self.dir = self.dirs[0 if self.type > 0 else 1]
        # 炮桶主向
        self.ptdir = self.dir
        # 敌方坦克每个方向移动步长
        self.step = random.randint(6, 20)
        self.x, self.y = self.rect.x, self.rect.y = x, y
        # 英雄坦克最大挂载子弹数量
        self.missile_max_number = 5
        # 英雄坦克挂载子弹时间
        self.interval = 0

    # 重写精灵更新方法
    # 参数：key 键盘按下或按住状态
    #      event 键盘松开状态
    def update(self, key, event=None):
        # 如果坦克为英雄1号
        if self.type is 1:
            if event is not None and event.key == K_j:
                # 按j键发射子弹
                self.fire()
            # 根据wasd键状态改变坦克方向
            if key[K_w]:
                self.dir = self.dirs[0]
            elif key[K_s]:
                self.dir = self.dirs[1]
            elif key[K_a]:
                self.dir = self.dirs[2]
            elif key[K_d]:
                self.dir = self.dirs[3]
            else:
                self.dir = self.dirs[4]
        elif self.type is 2:
            # 如果坦克为英雄2号，小键盘4发子弹，方向键改变方向
            if event is not None and event.key == K_KP4:
                self.fire()
            if key[K_UP]:
                self.dir = self.dirs[0]
            elif key[K_DOWN]:
                self.dir = self.dirs[1]
            elif key[K_LEFT]:
                self.dir = self.dirs[2]
            elif key[K_RIGHT]:
                self.dir = self.dirs[3]
            else:
                self.dir = self.dirs[4]
        else:
            # 否则，敌方坦克调用自动改变方向的方法
            self.turn_dir()
        if self.dir is not self.dirs[4]:
            # 如果坦克不是停止状态，炮桶方向跟坦克方向
            self.ptdir = self.dir
        # 根据坦克方向改变坦克图片
        self.image = self.images.get(self.ptdir)
        self.out()
        self.collide_wall()
        self.collide_tank()
        self.load_missile()
        # 移动坦克
        self.rect.move_ip(self.move())

    # 根据坦克方向计算坦克移动量
    def move(self):
        self.x, self.y = self.rect.x, self.rect.y
        x, y = 0, 0
        if self.dir is self.dirs[0]:
            y = -self.speed
        elif self.dir is self.dirs[1]:
            y = self.speed
        elif self.dir is self.dirs[2]:
            x = -self.speed
        elif self.dir is self.dirs[3]:
            x = self.speed
        return x, y

    # 改变敌方坦克方向
    def turn_dir(self):
        if self.type is 0:
            if self.step > 0:
                self.step -= 1
            else:
                # 随机生成前进步数
                self.step = random.randint(6, 20)
                # 随机生成前进方向
                self.dir = self.dirs[random.randrange(len(self.dirs))]
            if random.randrange(40) > 38:
                # 随机发射子弹
                self.fire()

    # def draw_super(self):
    #     if self.is_super:


    # 如果有子弹就就发子弹
    def fire(self):
        if self.missile_max_number > 0:
            # 已挂载有子弹才可发射
            # 生成一颗子弹
            self.tg.missiles.add(Missile(self, self.tg))
            # 生成一个子弹发射爆炸
            self.tg.bombs.add(Bomb(self.tg.images.fire, 'fire', self, self.tg))
            # 子弹数量-1
            self.missile_max_number -= 1
            if self.type > 0 and self.tg.is_mixer:
                # 英雄坦克发射子弹才播放音效
                self.tg.music.fire.play()

    # 判断超出边界
    def out(self):
        if self.rect.right > self.tg.width:
            self.rect.right = self.tg.width
        elif self.rect.left < 0:
            self.rect.left = 0
        if self.rect.bottom > self.tg.height:
            self.rect.bottom = self.tg.height
        elif self.rect.top < 0:
            self.rect.top = 0

    # 每20帧挂载一发炮弹
    def load_missile(self):
        if self.interval > 20:
            # 重新计时
            self.interval = 0
        else:
            # 时间递增
            self.interval += 1
        if self.interval == 20 and self.missile_max_number < 5:
            # 到时间挂载子弹，并且可装载子弹还有空间，增加一颗子弹
            self.missile_max_number += 1

    # 判断坦克是否挂了
    def is_over(self):
        self.tg.bombs.add(Bomb(self.tg.images.bomb, 'tank', self, self.tg))
        # 如果是英雄坦克
        if self.type > 0:
            if not self.is_super:
                if self.type is 1 and self.tg.hero1_num > 0 or self.type is 2 and self.tg.hero2_num > 0:
                    # 英雄1号或2号剩余坦克大于0，重置坦克位置
                    self.reset_pos()
                else:
                    # 否则移除坦克，并设置正在游戏中的英雄数量-1
                    self.tg.tanks.remove(self)
                    self.tg.hero_num -= 1
        else:
            # 如果是敌方坦克，直接移除，并减小敌方剩余坦克数量，主类中会根据坦克剩余量和运行中坦克生成新坦克
            self.tg.tanks.remove(self)
            # self.tg.enemy_num -= 1
            if self.tg.is_mixer:
                # 播放音效
                self.tg.music.bomb.play()

    # 重置英雄坦克
    def reset_pos(self):
        # 分别对英雄1号和2号坦克的位置进行重置，并减小坦克剩余量
        if self.type is 1:
            self.rect.x, self.rect.y = 400, 600
            self.tg.hero1_num -= 1
        elif self.type is 2:
            self.rect.x, self.rect.y = 520, 600
            self.tg.hero2_num -= 1

    # 坦克碰撞墙
    def collide_wall(self):
        collide_list = pygame.sprite.spritecollide(self, self.tg.walls, False, pygame.sprite.collide_mask)
        for wall in collide_list:
            if wall.type != 1:
                # 除了碰到草墙，碰到其它墙坦克均停止前进
                self.stay()

    # 坦克之间互相碰撞检测
    def collide_tank(self):
        collide_list = pygame.sprite.spritecollide(self, self.tg.tanks, False, pygame.sprite.collide_mask)
        for tank in collide_list:
            if self is not tank:
                # 如果是同一方坦克，各自回到上一步的位置
                if bool(tank.type) is bool(self.type):
                    tank.stay()
                    self.stay()
                else:
                    # 否则各自毁灭，调用is_over执行各种坦克的各自操作
                    # self.tg.bombs.add(Bomb(self.tg.images.bomb, 'tank', tank, tank.tg))
                    # self.tg.bombs.add(Bomb(self.tg.images.bomb, 'tank', self, self.tg))
                    tank.is_over()
                    self.is_over()

    # 坦克后退到上一步的位置
    def stay(self):
        self.rect.x, self.rect.y = self.x, self.y

class Superhero(pygame.sprite.Sprite):
    def __init__(self, tank, tg):
        pygame.sprite.Sprite.__init__(self)
        self.tg = tg
        self.tank = tank
        self.image = self.tg.images.super
        self.rect = self.tank.rect
        self.is_live = 500

    def update(self):
        self.rect = self.tank.rect
        if self.is_live == 0 :
            self.tank.is_super = False
            self.tg.reward.remove(self)
        else:
            self.is_live -= 1
