import pygame


class Bomb(pygame.sprite.Sprite):
    """爆炸类，继承pygame.sprite.Sprite精灵类
        参数:
        image 爆炸类图片(二维数组)
        name 爆炸类名称，识别坦克爆炸效果或坦克发子弹爆炸效果
        tank 产生爆炸的坦克
        tg main主类引用
    """

    def __init__(self, images, name, tank, tg):
        # 精灵类必须选运行精灵基类的__init__方法
        pygame.sprite.Sprite.__init__(self)
        self.tg, self.tank = tg, tank
        self.images = images
        self.image = self.images[0]  # 精灵类指定属性image
        self.rect = self.image.get_rect()  # 精灵类指定属性rect
        self.rect.center = self.tank.rect.center  # 指定爆炸位置
        self.step = 0  # 设置爆炸时长的起始数
        self.offset(name)

    # 精灵类方法重写，精灵调用此方法更新精灵
    def update(self):
        # 根据爆炸时长参数变换爆炸图片
        self.image = self.images[self.step // 4]
        if self.step == len(self.images) * 4 - 1:
            # 更换完所有图片，移除爆炸效果
            self.tg.bombs.remove(self)
        else:
            self.step += 1

    # 如果是发炮弹的爆炸效果，计算偏移
    def offset(self, name):
        # 如果爆炸类名称为发射子弹，通过坦克炮桶各个方向计算偏移，爆炸效果出现在炮口
        if name == 'fire':
            if self.tank.ptdir is self.tank.dirs[0]:
                self.rect.centery -= self.tank.rect.width / 2
            elif self.tank.ptdir is self.tank.dirs[1]:
                self.rect.centery += self.tank.rect.width / 2
            elif self.tank.ptdir is self.tank.dirs[2]:
                self.rect.centerx -= self.tank.rect.height / 2
            elif self.tank.ptdir is self.tank.dirs[3]:
                self.rect.centerx += self.tank.rect.height / 2
