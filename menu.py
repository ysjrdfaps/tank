import pygame


class Menu(pygame.sprite.Sprite):
    """
    菜单类
        参数：
        type 菜单类型，菜单排列的顺序，要根据这个顺序获取菜单按钮的图片
        x,y 菜单按钮的坐标
        tg main主类引用
    """

    def __init__(self, type, x, y, tg):
        pygame.sprite.Sprite.__init__(self)
        self.tg, self.type = tg, type
        self.unlock = True
        self.images = self.tg.images.menu
        self.image = self.images[self.type][0]
        self.rect = self.image.get_rect()
        self.rect.x = x
        self.rect.y = y

    # 重写精灵类的更新方法
    # 参数：show_menu 要显示的按钮类型
    #      event 事件，有可能为None
    def update(self, show_menu, event=None):
        # 根据按钮类型判断是否显示该按钮可用
        if self.type in show_menu:
            # 如果可用，取消锁定按钮为真
            self.unlock = True
            # 如果按钮类型不为音效4和背景音乐5
            if self.type != 4 and self.type != 5:
                # 判断鼠标是否在按钮上，是则更换显示图片
                if self.rect.collidepoint(pygame.mouse.get_pos()):
                    self.image = self.images[self.type][1]
                else:
                    self.image = self.images[self.type][0]
        else:
            # 否则，锁定按钮
            self.unlock = False
            self.image = self.images[self.type][2]
        if event is not None:
            # 有事件传入，并且事件不为空，把事件传到mouse方法中
            self.mouse(event)

    # event 事件
    def mouse(self, event):
        # 如果当前按钮不是锁定状态，并且判断是鼠标左键点击
        if self.unlock and self.rect.collidepoint(event.pos) and event.button is 1:
            if self.type is 0:
                # 如果按钮类型为开始游戏0，关闭显示菜单，并开始运行游戏
                self.tg.is_show_menu = False
                self.tg.is_run = True
            elif self.type is 1:
                # 如果按钮类型为继续游戏1，关闭显示菜单
                self.tg.is_show_menu = False
            elif self.type is 2:
                # 如果按钮类型为编辑地图2，切换关卡显示状态， 显示或隐藏关卡选择
                self.tg.is_show_checkpoint = not self.tg.is_show_checkpoint
            elif self.type is 3:
                # 如果按钮类型为退出游戏3，退出游戏
                self.tg.quit()
            elif self.type is 4:
                # 如果按钮类型为4，音效开关，把音效状态取反，并更换音效开关图片
                self.tg.is_mixer = not self.tg.is_mixer
                self.image = self.images[self.type][int(not self.tg.is_mixer)]
            elif self.type is 5:
                # 如果按钮类型为5，音乐开关，把音乐状态取反，并更换音乐开关图片
                self.tg.is_music = not self.tg.is_music
                self.image = self.images[self.type][int(not self.tg.is_music)]
                # 判断如果音乐关，并且正在播放音乐，则关闭音乐
                if not self.tg.is_music and self.tg.game.mixer.music.get_busy():
                    self.tg.game.mixer.music.stop()
                    # 否则如果音乐开，并且游戏在运行状态，重新启动音乐
                elif self.tg.is_music and self.tg.is_run:
                    self.tg.game.mixer.music.play()


class Checkpoint(pygame.sprite.Sprite):
    """
    菜单中的关卡显示类，正考虑与菜单类合并
        参数：
        type 菜单类型，菜单排列的顺序，要根据这个顺序获取菜单按钮的图片
        x,y 按钮的坐标
        tg main主类引用
    """

    def __init__(self, type, x, y, tg):
        pygame.sprite.Sprite.__init__(self)
        self.tg, self.type = tg, type
        self.images = self.tg.images.checkpoint
        self.image = self.images[self.type][0]
        self.rect = self.image.get_rect()
        self.rect.x, self.rect.y = x, y

    # 重写精灵类的更新方法
    # 参数：event 有可能为None
    def update(self, event=None):
        # 判断鼠标是否移动到按钮上，是则更换图片
        if self.rect.collidepoint(pygame.mouse.get_pos()):
            self.image = self.images[self.type][1]
        else:
            self.image = self.images[self.type][0]
        # 如果事件不为None，把事件传到mouse方法中
        if event is not None:
            self.mouse(event)

    def mouse(self, event):
        # 如果点击了关卡按钮
        if self.tg.is_show_checkpoint and self.rect.collidepoint(event.pos) and event.button is 1:
            # 改变要编辑的关卡
            self.tg.edit_checkpoint = self.type
            # 关闭关卡选择框
            self.tg.is_show_checkpoint = False
            # 关闭菜单
            self.tg.is_show_menu = False
            # 显示编辑地图
            self.tg.is_edit_map = True
