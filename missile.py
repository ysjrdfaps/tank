import pygame

from bomb import Bomb


class Missile(pygame.sprite.Sprite):
    """
    子弹类
        参数：
        tank 发身子弹的坦克引用
        tg main主类引用
    """

    def __init__(self, tank, tg):
        pygame.sprite.Sprite.__init__(self)
        self.tg, self.tank = tg, tank
        self.type, self.dir, self.img_num = self.tank.type, self.tank.ptdir, self.tank.img_num
        self.images = self.tg.images.missile[self.img_num]
        self.speed = self.tank.speed * 2
        self.image = self.images.get(self.dir)
        self.rect = self.image.get_rect()
        # 使用mask进行碰撞检测，即忽略透明部分
        self.mask = pygame.mask.from_surface(self.image)
        self.rect.center = self.tank.rect.center

    # 重写精灵的更新方法
    def update(self):
        # 调用rect的移动方法，移动子弹
        self.rect.move_ip(self.move())
        # 检测与坦克碰撞
        self.hit_tank()
        # 检测与墙碰撞
        self.hit_wall()
        # 检测是否超出边界
        self.out()

    # 子弹移动
    def move(self):
        # 根据子弹方向调整子弹移动的坐标
        x, y = 0, 0
        if self.dir is self.tank.dirs[0]:
            y = -self.speed
        elif self.dir is self.tank.dirs[1]:
            y = self.speed
        elif self.dir is self.tank.dirs[2]:
            x = -self.speed
        elif self.dir is self.tank.dirs[3]:
            x = self.speed
        return x, y

    # 判断是否打中坦克
    def hit_tank(self):
        # 用当前的子弹与所有坦克进行碰撞检测
        collide_list = pygame.sprite.spritecollide(self, self.tg.tanks, False, pygame.sprite.collide_mask)
        for tank in collide_list:
            if bool(tank.type) is not bool(self.tank.type):
                # 区分子弹为我方和敌方，如果不是同一方，添加一个坦克爆炸效果
                # self.tg.bombs.add(Bomb(self.tg.images.bomb, 'tank', tank, self.tg))
                # 移除子弹
                self.tg.missiles.remove(self)
                # 调用坦克的方法判断是否移除坦克
                tank.is_over()

    # 判断是否打中墙
    def hit_wall(self):
        # 用当前的子弹与所有墙进行碰撞检测
        collide_list = pygame.sprite.spritecollide(self, self.tg.walls, False, pygame.sprite.collide_mask)
        for wall in collide_list:
            if wall.type is 0:
                # 如果墙类型为土墙0，移除子弹，移除墙
                self.tg.missiles.remove(self)
                self.tg.walls.remove(wall)
            elif wall.type is 3:
                # 如果墙类型为钢墙3，移除子弹
                self.tg.missiles.remove(self)
            elif wall.type is 4 and self.tank.type is 0:
                # 如果墙类型为总部4，并且为敌方坦克，移除子弹
                self.tg.missiles.remove(self)
                # 移除总部
                self.tg.walls.remove(wall)
                # 把英雄1P2P的数量设置为0，即结束游戏
                self.tg.hero1_num = self.tg.hero2_num = 0

    # 判断子弹出界，出界则移除子弹
    def out(self):
        if self.rect.right > self.tg.width or self.rect.left < 0 or self.rect.bottom > self.tg.height or self.rect.top < 0:
            self.tg.missiles.remove(self)
