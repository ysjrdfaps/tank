import pygame


class Wall(pygame.sprite.Sprite):
    """
    墙类
        参数：
        tg main主类引用
        wall 墙的属性，包括类型，坐标
    """

    def __init__(self, tg, wall):
        pygame.sprite.Sprite.__init__(self)
        self.tg = tg
        self.type, self.x, self.y = wall
        self.images = self.tg.images.wall
        self.image = self.images[self.type]
        self.rect = self.image.get_rect()
        # self.mask = pygame.mask.from_surface(self.image)
        self.rect.x, self.rect.y = self.x, self.y
