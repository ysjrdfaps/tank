import random

import pygame

from bomb import Bomb
from tank import Superhero
from wall import Wall


class Reward(pygame.sprite.Sprite):
    """
    随机物品类
        参数：
        tg main主类引用
    """

    def __init__(self, tg):
        pygame.sprite.Sprite.__init__(self)
        self.tg = tg
        # 随机生成物品类型
        self.type = random.randrange(len(self.tg.images.reward))
        # 获取物品图片，二维数组
        self.images = self.tg.images.reward
        # 根据类型指定物品图片
        self.image = self.images[self.type]
        self.rect = self.image.get_rect()
        # 随机生成位置
        self.rect.x = random.randrange(self.tg.width - 40)
        self.rect.y = random.randrange(self.tg.height - 40)
        # 随机物品的生命周期
        self.live = 1000

    # 重写精灵更新方法
    def update(self):
        if self.live < 0:
            # 生命小于0，随机物品消失
            self.tg.reward.remove(self)
        elif self.live < 160:
            # 生命小于160，随机物品开始闪烁
            if self.live % 8 > 3:
                self.image = self.tg.images.blank
            else:
                self.image = self.images[self.type]
            self.live -= 1
        else:
            # 生命不断递减
            self.live -= 1
        self.collide_tank()

    # 判断与英雄坦克碰撞
    def collide_tank(self):
        collide_list = pygame.sprite.spritecollide(self, self.tg.tanks, False)
        for tank in collide_list:
            # 碰撞到英雄坦克，即为获取成功
            if tank.type > 0:
                if self.tg.is_music:
                    # 若允许播放音效就播放音效
                    self.tg.music.get_reward.play()
                if self.type is 0:
                    self.health_kit(tank)
                elif self.type is 1:
                    self.box()
                elif self.type is 2:
                    self.barrel()
                elif self.type is 3:
                    self.reinforce(tank)
                # 获取成功，物品消失
                self.tg.reward.remove(self)

    def health_kit(self, tank):
        # 物品类型为0，即增加英雄坦克剩余数量
        if tank.type is 1 and self.tg.hero1_num < 5:
            # 英雄剩余数量小于5才进行增加
            self.tg.hero1_num += 1
        if tank.type is 2 and self.tg.hero2_num < 5:
            self.tg.hero2_num += 1

    def box(self):
        # 物品类型为1，修复总部周边墙
        for w in self.tg.home:
            temp = True
            for wall in self.tg.walls:
                # 判断没有墙才进行修改，有墙不理会
                if w[1] == wall.x and w[2] == wall.y:
                    temp = False
                    break
            if temp:
                self.tg.walls.add(Wall(self.tg, w))

    def barrel(self):
        # 取出所有坦克
        for tank in self.tg.tanks:
            if tank.type is 0:
                # 如果是敌方坦克，全爆
                self.tg.bombs.add(Bomb(self.tg.images.bomb, 'tank', tank, self.tg))
                tank.is_over()

    def reinforce(self,tank):
        if not tank.is_super:
            tank.is_super = True
            self.tg.reward.add(Superhero(tank, self.tg))